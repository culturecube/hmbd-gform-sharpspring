<?php
/**
 *Plugin Name: HMBD Gform SharpSpring
 *Description: Integrating gravity forms on HMBD website with SharpSpring
 *Author: Matt n' Greg
 *Version: 1.0.0
**/

    add_action( 'gform_after_submission_4', 'post_to_third_party_4', 10, 2 );

    function post_to_third_party_4( $entry, $form ) {
        $baseURI = 'https://app-3QNDCC1UMY.marketingautomation.services/webforms/receivePostback/MzawMDEzNTcxBgA/';
        $endpoint = 'dcd6d9ae-0a21-4ad3-8612-db55f2bfeba2';
        $post_url = $baseURI . $endpoint;

        $body = array(
            'Name of Insured' => rgar( $entry, '1' ),
            'Email' => rgar( $entry, '2' ),
            'Phone Number to Contact' => rgar( $entry, '8' ),
            'Type of Policy' => rgar( $entry, '3' ),
            'Policy Number' => rgar( $entry, '4' ),
            'Service' => rgar( $entry, '7' ),
            'Comments' => rgar( $entry, '6' ),
            'trackingid__sb' => $_COOKIE['__ss_tk']
        );
        $request = new WP_Http();

        $response = $request->post( $post_url, array( 'body' => $body ) );
    } 
?>